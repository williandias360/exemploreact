import React from 'react'
import { View, Text } from 'react-native'
import Padrao from '../estilo/Padrao'
import padrao from '../estilo/Padrao';

function parOuImpar(num) {
    // if (num % 2 == 0)
    //     return <Text style={padrao.ex}>Par</Text>
    // else <Text style={padrao.ex}>Impar</Text>
    return <Text style={padrao.ex}>{num % 2 == 0 ? 'Par' : 'Impar'}</Text>
}

export default props =>
    <View>
        {
            parOuImpar(props.numero)
        }
    </View>
